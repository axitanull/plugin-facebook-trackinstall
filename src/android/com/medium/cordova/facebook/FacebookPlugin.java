package com.medium.cordova.facebook;

import android.content.Context;

import org.apache.cordova.CordovaPlugin;

import com.facebook.appevents.AppEventsLogger;

public class FacebookPlugin extends CordovaPlugin
{
   /**
    * Gets the application context from cordova's main activity.
    * 
    * @return the application context
    */
   private Context getApplicationContext()
   {
      return this.cordova.getActivity().getApplicationContext();
   }

   @Override
   public void onPause(boolean multitasking)
   {
      super.onPause(multitasking);

      AppEventsLogger.deactivateApp(getApplicationContext());
   }

   @Override
   public void onResume(boolean multitasking)
   {
      super.onResume(multitasking);

      AppEventsLogger.activateApp(getApplicationContext());
   }
}
