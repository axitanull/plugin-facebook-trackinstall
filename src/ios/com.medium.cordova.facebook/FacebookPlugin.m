#import "FacebookPlugin.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface FacebookPlugin ()
@end

@implementation FacebookPlugin

- (CDVPlugin *)initWithWebView:(UIWebView *)webview {
    self = (FacebookPlugin *)[super initWithWebView:webview];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidFinishLaunching:)
                                                 name:UIApplicationDidFinishLaunchingNotification 
                                               object:nil];

    return self;
}

- (void)applicationDidBecomeActive {
    [FBSDKAppEvents activateApp];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    NSDictionary *launchOptions = [notification userInfo] ;

    [[FBSDKApplicationDelegate sharedInstance] application:[UIApplication sharedApplication] 
                             didFinishLaunchingWithOptions:launchOptions];
}

@end
